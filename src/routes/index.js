'use strict';

var express = require('express');
var router = express.Router();
var auth = require("../config/auth");
var controller = require('../controllers/index.js');

router.get('/', controller.index);
router.get('/users', controller.users);
router.get('/getUser', auth, controller.getUser);
router.get('/home', auth, controller.home);
router.post('/login', controller.login);
router.post('/signup', controller.signup);
router.post('/logout', controller.logout);

module.exports = router;

