'use strict';

var User = require('../models/user.js');

function index(req, res){
    console.log('Estoy en index');
    res.redirect('/login');
}

async function users(req, res){
    let users = await User.find().sort({'username': 1});
    res.json(users);
}

async function login(req, res) {
    console.log('Estoy en loginController');

    try {
        
        function myError(message) {
            this.message = message;
        }
        myError.prototype = new Error();

        var email = req.body.email;
        var password = req.body.password;
        var user = await User.findByCredentials(email, password);

        if (!user) {
            //return res.status(401).json({ error: 'Login failed! Check authentication credentials' });
            return res.status(401).json({ myError });
        }else{
            var token = await user.generateAuthToken();
            res.status(201).json({ user, token });
        }
    } catch (err) {
        res.status(400).json({ error: err });
    }
}

async function signup(req, res, next) {
    console.log('Estoy en signup');
    try {
        let isUser = await User.find({ email: req.body.email });
        console.log(isUser);
        if (isUser.length >= 1) {
            return res.status(409).json({
                message: 'email already in use'
            });
        }
        const user = new User({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        });
        console.log('user',user)
        let data = await user.save();
        const token = await user.generateAuthToken(); // here it is calling the method that we created in the model
        res.status(201).json({ data, token });
    } catch (err) {
        res.status(400).json({ err: err });
    }
}

async function getUser(req, res){
    console.log('Estoy en getUser');
    await res.json(req.userData);
}

async function home(req, res){
    console.log('Estoy en home');
    await res.json(req.userData);
}

function logout(req, res) {
    req.logout();
    res.redirect('/');
}

module.exports = {
    index,
    users,
    login,
    signup,
    getUser,
    home,
    logout
};
